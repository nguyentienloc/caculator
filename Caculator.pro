#-------------------------------------------------
#
# Project created by QtCreator 2017-07-11T04:05:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Caculator
TEMPLATE = app


SOURCES += cCaculator/main.cpp \
    cCaculator/mainwindow.cpp

HEADERS  += \
    cCaculator/mainwindow.h

FORMS    += uiWidget/mainwindow.ui

target.path=/home/root/styl_test/copyfile
INSTALLS += target
