#include "mainwindow.h"
#include <QApplication>
#include <QString>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if (a.arguments().count() == 2)
    {
        if (QString::compare(a.arguments().at(1), "--version") == 0)
        {
            QTextStream out(stdout);
            out << "version 1.0.0";
            return 0;
        }
    }

    MainWindow w;
    w.showFullScreen();

    return a.exec();
}


