#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QDebug>

enum   math_operator{SUM,SUB,MUL,DIV,NONE};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn0_clicked();
    void on_btn1_clicked();
    void on_btn2_clicked();
    void on_btn3_clicked();
    void on_btn4_clicked();
    void on_btn5_clicked();
    void on_btn6_clicked();
    void on_btn7_clicked();
    void on_btn8_clicked();
    void on_btn9_clicked();
    void on_btnClear_clicked();

    void on_btnSum_clicked();

    void on_btnSub_clicked();

    void on_btnMul_clicked();

    void on_btnDiv_clicked();

    void on_btnEqual_clicked();

private:
    Ui::MainWindow *ui;
    unsigned long m_Number1;
    unsigned long m_Number2;
    qint32 m_NumberCount;
    math_operator m_Operator;
    void catchInput(int iNumber,int position);
};

#endif // MAINWINDOW_H
