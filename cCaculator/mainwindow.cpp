#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_Number1 =0;
    m_Number2 =0;
    m_NumberCount = 1;
    m_Operator = NONE;
    // setColor
    ui->btn0->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn1->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn2->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn3->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn4->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn5->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn6->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn7->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                            "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn8->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                             "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btn9->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                             "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                            "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnClear->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                                "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                                "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnDiv->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                              "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                              "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnSub->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                              "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                              "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnSum->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                              "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                              "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnMul->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                              "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                              "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->btnEqual->setStyleSheet("QPushButton{background-color:black;color:white;font-size:60px}"
                                "QPushButton:hover{background-color:blue;color:white;font-size:60px}"
                                "QPushButton:pressed{background-color:blue;color:white;font-size:60px}");
    ui->edtResult->setStyleSheet("font-size:60px;text-align:left");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::catchInput(int iNumber, int position)
{
    switch(position)
    {
    case 1: m_Number1 = m_Number1*10 + iNumber;
            ui->edtResult->setText(QString::number(m_Number1));
        break;
    case 2:
            m_Number2 = m_Number2*10 + iNumber;
            ui->edtResult->setText(QString::number(m_Number2));
        break;

    }

}

void MainWindow::on_btn0_clicked()
{
    catchInput(0,m_NumberCount);
}

void MainWindow::on_btn1_clicked()
{
    catchInput(1,m_NumberCount);
}

void MainWindow::on_btn2_clicked()
{
    catchInput(2,m_NumberCount);
}

void MainWindow::on_btn3_clicked()
{
    catchInput(3,m_NumberCount);
}

void MainWindow::on_btn4_clicked()
{
    catchInput(4,m_NumberCount);
}

void MainWindow::on_btn5_clicked()
{
    catchInput(5,m_NumberCount);
}

void MainWindow::on_btn6_clicked()
{
    catchInput(6,m_NumberCount);
}

void MainWindow::on_btn7_clicked()
{
    catchInput(7,m_NumberCount);
}

void MainWindow::on_btn8_clicked()
{
    catchInput(8,m_NumberCount);
}

void MainWindow::on_btn9_clicked()
{
    catchInput(9,m_NumberCount);
}

void MainWindow::on_btnClear_clicked()
{
    m_Number1 = 0;
    m_Number2 = 0;
    m_NumberCount = 1;
    ui->edtResult->setText("");
}

void MainWindow::on_btnSum_clicked()
{
    m_NumberCount = 2;
    ui->edtResult->setText("+");
    m_Operator = SUM;
}

void MainWindow::on_btnSub_clicked()
{
    m_NumberCount = 2;
    ui->edtResult->setText("-");
    m_Operator = SUB;
}

void MainWindow::on_btnMul_clicked()
{
    m_NumberCount = 2;
    ui->edtResult->setText("x");
    m_Operator = MUL;
}

void MainWindow::on_btnDiv_clicked()
{
    m_NumberCount = 2;
    ui->edtResult->setText("/");
    m_Operator = DIV;
}

void MainWindow::on_btnEqual_clicked()
{
    double result=0;
    switch (m_Operator) {
    case SUM:
        result = m_Number1 + m_Number2;
        break;
    case SUB:
        result = m_Number1 - m_Number2;
        break;
    case MUL:
        result = m_Number1 * m_Number2;
        break;
    case DIV:
        result = (double)m_Number1 / m_Number2;
        //qDebug() << __FUNCTION__ result;
        break;
    default:
        break;
    }
    ui->edtResult->setText(QString::number(result,'g',10));
}
